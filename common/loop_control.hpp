#pragma once

#include <list>
#include <exception>
#include <stdexcept>
#include "configuration.hpp"
#include "mpi_environment.hpp"

/*
 * Main loop control: coordinate between the MPI processes to quit at the same time step, throttle enqueuing of kernels,
 * check termination conditions.
 * This struct can be converted to uint64_t to get the current time step.
 */

struct loop_control {

	operator uint64_t() const { return t; }
	uint64_t operator*() const { return t; }
	uint64_t operator+=(uint64_t steps) { return t += steps; }

	bool break_now() const {
		if (quit_requested && !synched) synch_max();
		return gconf.steps == t && (synched || gconf.steps);
	}

protected:
	uint64_t t = gconf.time_offset;
	mutable bool synched = false;
	//the max step reduction is performed independently from all other calculations
	const boost::mpi::communicator mpi_global_alt{ mpi_global, boost::mpi::comm_duplicate };
	void synch_max() const {
		if (synched) throw std::logic_error("max time step has already been synchronized");
		boost::mpi::all_reduce(mpi_global_alt, t, gconf.steps, boost::mpi::maximum<uint64_t>());
		synched = true;
	}
};
