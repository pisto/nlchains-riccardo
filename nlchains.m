(* ::Package:: *)

(* ::Input::Initialization:: *)
BeginPackage["nlchains`"];


(* ::Input::Initialization:: *)
LoadDump::usage="LoadDump[\"filename\", n] loads an ensemble of chains of length n for a real model (pairs of coordinates and momenta) from the specified file";
LoadEntropy::usage="LoadEntropy[\"filename\"] loads an entropy file with triplets containing the time stamp, the Wave Turbulence entropy and the information entropy";
LoadLinearEnergies::usage="LoadLinearEnergies[\"filename\"] loads a \"linenergies\" file with the linear energies of a model, returning a list of pairs of wave number and corresponing linear energy";
EnergydFPUT::usage="EnergyFPUT[\[Phi]\[Pi], ks, \[Alpha], \[Beta]] returns the total energy for a realization \[Phi]\[Pi] of the dFPUT model, with elastic constants ks, cubic and quartic nonlinear parameters \[Alpha] and \[Beta]";
EigendFPUT::usage="EigendFPUT[ks] returns the Eigensystem associated to the FPUT system with elast constants ks";
LinearEnergiesFPUT::usage="LinearEnergiesFPUT[\[Phi]\[Pi], eigens] returns the linear modes energy of the FPUT system with state \[Phi]\[Pi] and Eigenvalues eigens";
EntropyINF::usage="EntropyINF[e] returns the information entropy for the list of linear energies e";
EntropyWT::usage="EntropyWT[e] returns the Wave Turbulence entropy for the list of linear energies e";


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Input::Initialization:: *)
LoadDump[name_String,n_Integer]:=With[{l=BinaryReadList[name,{"Real64","Real64"}]},ArrayReshape[l,{Length@l/n,n,2}]]
LoadEntropy[name_String]:=With[{l=BinaryReadList[name,"Real64"]},ArrayReshape[l,{Length@l/3,3}]]
LoadLinearEnergies[name_String]:=BinaryReadList[name,"Real64"]


(* ::Input::Initialization:: *)
EnergydFPUT[\[Phi]pi_?ArrayQ,ks_?ArrayQ,\[Alpha]_,\[Beta]_]/;MatchQ[Dimensions@\[Phi]pi,{_,2}]&&Length@ks==Length@\[Phi]pi:=With[{diffs=ListConvolve[{1,-1},\[Phi]pi[[All,1]],{-1,-1}]},
Total[{\[Phi]pi[[All,2]]^2/2+ks diffs^2/2,\[Alpha]/3diffs^3,\[Beta]/4diffs^4},{2}]]


(* ::Input::Initialization:: *)
EigendFPUT[k_?ArrayQ]/;ArrayDepth@k==1:=Sort@Transpose@MapAt[Sqrt@*Minus,Quiet@Eigensystem@SparseArray@{Band@{1,1}->-(k+RotateRight@k),Band@{1,2}->Most@k,Band@{2,1}->Most@k,{1,-1}->Last@k,{-1,1}->Last@k},1]


(* ::Input::Initialization:: *)
LinearEnergiesFPUT[\[Phi]pi_?ArrayQ,eigens_List]/;MatchQ[{Dimensions@\[Phi]pi,Length@eigens[[1,2]]},{{n_,2},n_}]:=((#[[1]](\[Phi]pi[[All,1]].#[[2]]))^2+(\[Phi]pi[[All,2]].#[[2]])^2&/@eigens)/2


(* ::Input::Initialization:: *)
EntropyINF[energies_?ArrayQ]/;ArrayDepth@energies==1:=With[{fk=Select[energies,Not@*PossibleZeroQ]Length@energies/Total@energies},Total[fk Log@fk]]
EntropyWT[energies_?ArrayQ]/;ArrayDepth@energies==1:=With[{fk=energies Length@energies/Total@energies},-Total[Log@fk]]


(* ::Input::Initialization:: *)
End[];


(* ::Input::Initialization:: *)
EndPackage[]
